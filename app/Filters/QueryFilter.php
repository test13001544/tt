<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class QueryFilter
{
    private Request $request;
    protected Builder $builder;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    private function getQueryParam(): array
    {
        return $this->request->query();
    }

    public function apply(Builder $builder): Builder
    {
        $this->builder = $builder;

        foreach ($this->getQueryParam() as $nameFunction => $param) {
            if (method_exists($this, $nameFunction)) {
                $this->$nameFunction($param);
            }
        }

        return $this->builder;
    }

    protected function getComparisonOperator($arr, string $comparisonOperator = '='): string
    {
        if (count($arr) > 2) {
            $comparisonOperator = $arr[1];
        }
        return $comparisonOperator;
    }
}
