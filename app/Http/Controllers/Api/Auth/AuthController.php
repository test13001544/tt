<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\Register\StoreRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function register(StoreRequest $request): JsonResponse
    {
        User::query()->create($request->validated());
        $token = auth()->attempt($request->validated());

        return $this->respondWithToken($token, config('jwt.ttl'));
    }

    public function login(\App\Http\Requests\Api\Auth\Login\StoreRequest $request): JsonResponse
    {
        if (!$token = auth()->attempt($request->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token, config('jwt.ttl'));
    }

    public function logout(): JsonResponse
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh(): JsonResponse
    {
        try {
            return $this->respondWithToken(auth()->refresh(), config('jwt.refresh_ttl'));
        } catch (\Throwable) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
    }

    protected function respondWithToken(string $token, int $timeConfig): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => $timeConfig * 60
        ]);
    }
}
