<?php

namespace App\Http\Requests\Api\Form;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'subject' => ['required','min:5','max:50'],
            'message' => ['required','min:5','max:250'],
        ];
    }
}
