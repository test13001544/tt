<?php

namespace Tests\Unit\Api\Models;

use App\Models\Form;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Traits\Tests\AuthUser;

class FormTest extends TestCase
{
    use RefreshDatabase;
    use AuthUser;

    public function test_create_form(): void
    {
        $this->withToken($this->token)->post('/api/forms', [
            'subject' => 'subject',
            'message' => 'message',
        ])->assertSuccessful(); // создать сущность удалось
        $this->assertEquals(1, Form::all()->count()); // создана только одна сущность
        $this->assertEquals(Form::query()->first()->status, 'new'); // 'status' default: new
        $this->assertEquals(Form::query()->first()->user_id, auth()->user()->id); // 'user_id' устанавливается корректно
    }

    public function test_update_form(): void
    {
        $form = Form::query()->create([ //factories/seeders
            'subject' => 'subject',
            'message' => 'message',
        ]);
        $this->editRoleUserOnManager();

        $this->withToken($this->token)->patch('/api/forms/' . $form->id, [
            'comment' => 'comment',
        ])->assertSuccessful(); // изменить сущность удалось
        $this->assertNotEquals($form->comment, Form::find($form->id)->comment); // 'comment' до/после !=
        $this->assertEquals(Form::find($form->id)->comment, 'comment'); // 'comment'  после изменений == вносимым данным
        $this->assertEquals(Form::find($form->id)->status, 'in progress'); // 'status' default: 'new' изменен после update
    }
}

// TODO: Писть можно много и много
